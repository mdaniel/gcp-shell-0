
locals {
  # the permissions that will be assigned to the Organization Role
  # which is used for discovery
  org_roles = [
    "roles/cloudasset.viewer",
    "roles/spanner.viewer",
  ]

  # the permissions that will be assigned to the project-local Role
  project_roles = [
    "roles/cloudfunctions.invoker",
  ]

  # the services to be enabled in the newly created Project
  project_services = [
    "cloudbilling.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "iam.googleapis.com",
    "iamcredentials.googleapis.com",
    "serviceusage.googleapis.com",
    "sts.googleapis.com",
  ]
}

data "google_iam_role" "org_roles" {
  for_each = toset(local.org_roles)
  name     = each.key
}

data "google_iam_role" "project_roles" {
  for_each = toset(local.project_roles)
  name     = each.key
}

# confirmed that these role_ids are namespaced
# https://console.cloud.google.com/iam-admin/roles/details/organizations%3C000011112222%3Croles%3Cmd20221216?organizationId=000011112222&orgonly=true&supportedpurview=organizationId
# https://console.cloud.google.com/iam-admin/roles/details/projects%3Cproject-000000%3Croles%3Cmd20221216?project=project-000000
resource "google_organization_iam_custom_role" "r" {
  # Error: "role_id" ("orvn-00cafebabe-cross-account") doesn't match regexp "^[a-zA-Z0-9_\\.]{3,64}$"
  role_id     = "ORVN_${var.openraven_org_id}"
  org_id      = var.gcp_org_id
  title       = "Open Raven cross-account Discovery Role"
  description = "the Role used by Open Raven to investigate Google Cloud resources"
  permissions = flatten([for rr in data.google_iam_role.org_roles : rr.included_permissions])
}

resource "google_organization_iam_binding" "org_binding" {
  org_id = var.gcp_org_id
  role   = google_organization_iam_custom_role.r.name
  members = [
    "serviceAccount:${var.openraven_sa_email}",
  ]
}

resource "google_project_iam_custom_role" "r" {
  role_id     = "ORVN_${var.openraven_org_id}"
  project     = google_project.openraven.id
  title       = "Open Raven cross-account Role"
  description = "the Role used by Open Raven to launching Cloud Functions"
  permissions = flatten([for rr in data.google_iam_role.project_roles : rr.included_permissions])
}

resource "google_project_iam_binding" "project_binding" {
  project = google_project.openraven.id
  role    = google_project_iam_custom_role.r.name
  members = [
    "serviceAccount:${var.openraven_sa_email}",
  ]
}

resource "google_project_service" "project" {
  for_each           = toset(local.project_services)
  project            = google_project.openraven.id
  disable_on_destroy = true
  service            = each.key
}
