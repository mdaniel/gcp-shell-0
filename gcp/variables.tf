variable "openraven_org_id" {
  type        = string
  description = "the Open Raven Org ID from the 'Add new GCP Account' screen in your workspace"
}

variable "openraven_sa_email" {
  type        = string
  description = "the Open Raven Service Account email address from the 'Add new GCP Account' screen in your workspace"
}

variable "gcp_org_id" {
  type        = string
  description = "the GCP Organization ID ([0-9]+)"
}

variable "gcp_folder_id" {
  type        = string
  description = "the (optional) Folder ID in which the new Project will live"
  default     = null
}

variable "gcp_billing_account_id" {
  type        = string
  description = "the Billing Account ID to associate with the new Project"
}
