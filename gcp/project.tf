resource "google_project" "openraven" {
  # no, the org_id is implied by the folder_id, so only provide it if folder_id is null
  org_id          = var.gcp_folder_id == null ? null : var.gcp_org_id
  folder_id       = var.gcp_folder_id
  name            = "ORVN ${var.openraven_org_id}"
  project_id      = "orvn-${var.openraven_org_id}"
  billing_account = var.gcp_billing_account_id
}
