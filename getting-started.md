# My Cloud Shell Tutorial

## Getting Started

This process will configure your Google Cloud Project to (...TBD...)

Click the **Start** below to begin the process

<!-- in minutes -->
<walkthrough-tutorial-duration duration="3.25"></walkthrough-tutorial-duration>

<!--
this just shows a button that allows opening a _new_ cloud shell terminal
and in this context, it opens next to our getting-started one

<walkthrough-open-cloud-shell-button></walkthrough-open-cloud-shell-button>
/-->

## Select Your Project

<!-- I don't think we need billing for this, since it's the **downstream** project that requires running things -->
<walkthrough-project-setup></walkthrough-project-setup>

## Show Your Project

_Project Id_:
<walkthrough-project-id/>

_Project Name_:
<walkthrough-project-name/>

## Commands

First, align yourself and `gcloud` to ensure things are as you wish.

### Authorization and Project Selection

<walkthrough-footnote>
Clicking the tiny terminal icon above the rendering of these commands will open an html "dialog"
to allow Cloud Shell to use your credentials. Unknown what perms it wants, since the dialog doesn't say.
</walkthrough-footnote>

Be sure you are executing as the user you expect:
```bash
gcloud auth list
```

Because Cloud Shell is launched outside of a project, you will need a value to provide to it

This selection is only for choosing the existing Project from which one can create resources in Google Cloud.
It is **not** the Project that Open Raven will connect to, as we are going to create that Project during this process.

To see the values available to your current user:
```bash
gcloud projects list --format=json | jq -r '.[].projectId' | sort
```

Then pick your favorite:
```bash
read -p 'projectId? ' MY_PROJECT_ID

echo '... then tell gcloud sdk about that choice:'
gcloud config set project $MY_PROJECT_ID
```

These are some pretty standard APIs that you will need to be successful.  The
upcoming command will have a placeholder of `--project-id <PROJECT-ID>` because the tutorial
framework does not know what Project you are currently using.

You can just **delete that text**, as it will use the default you just set.

Be aware these APIs are for the configuration process itself, and may differ from the Open Raven Discovery Role and the Open Raven Cloud Functions Role that are going to be created in a few minutes.

<walkthrough-enable-apis apis="
cloudbilling.googleapis.com,
cloudresourcemanager.googleapis.com,
iam.googleapis.com,
iamcredentials.googleapis.com,
serviceusage.googleapis.com,
sts.googleapis.com
"></walkthrough-enable-apis>

### gcloud config

First, show what `gcloud` thinks is going on

```bash
gcloud config list
echo "GOOGLE_CLOUD_PROJECT is '$GOOGLE_CLOUD_PROJECT'"
echo "CLOUD_SHELL is '$CLOUD_SHELL'"
```


## Show what's up

So, the `resource-manager folders list` that you're about to run does not tolerate the "fully qualified" version of the GCP Org Id
so grab the current `ORG_ID` for use:

```bash
MY_ORG_ID=$(gcloud organizations list --format=json | jq -r '.[0].name|split("/")[1]')
```

You will need to specify the Google Cloud Billing Account Id to use with the newly created Project.

If you don't know your Billing Account Id then
https://console.cloud.google.com/billing?project= _(append your project id there)_
will show you

You can also try this beta `gcloud` command:
```bash
CURRENT_BILLING_ACCOUNT_ID=$(gcloud beta billing projects describe $MY_PROJECT_ID --format=json | jq -r '.billingAccountName|split("/")[1]' || true)
echo "The current Billing Account ID is '$CURRENT_BILLING_ACCOUNT_ID'"
```

Please enter the Billing Account Id
```bash
read -p 'GCP Billing Account ID? ' TF_VAR_gcp_billing_account_id
export TF_VAR_gcp_billing_account_id
```

Now, show the folders:

```bash
gcloud resource-manager folders list --organization $MY_ORG_ID
```
That will influence where in the Organization one wishes to place the newly created Project for use by Open Raven,
if you wish for it to live in a Folder.

**Only do this** if you want the newly created Project in a folder:

```bash
read -p 'Folder id for the new Project? ' TF_VAR_gcp_folder_id
export TF_VAR_gcp_folder_id
```

## Create the Project, IAM Roles, and their Bindings

You will have received the _Open Raven Org Id_ from the account connection dialog in the project.
Please provide it here so your Google Cloud Organization can trust the Open Raven installation.

Same with the Service Account email address, which should have been on that same screen.

```bash
read -p 'Open Raven Org Id? ' TF_VAR_openraven_org_id
export TF_VAR_openraven_org_id
read -p 'Open Raven Service Account email? ' TF_VAR_openraven_sa_email
export TF_VAR_openraven_sa_email

export TF_VAR_gcp_org_id="$MY_ORG_ID"
mv *.tf gcp/
mv .terraform* gcp/
cd gcp
terraform init
terraform plan -out plan.out
```
If that output looks ok:

```bash
terraform apply plan.out
```

## All Done!

<walkthrough-conclusion-trophy></walkthrough-conclusion-trophy>

You can just close this browser window because there's no obvious way to shut-down a Cloud Shell
