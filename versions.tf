terraform {
  required_version = "~> 1.0"
  required_providers {
    google = {
      source = "hashicorp/google"
      # https://registry.terraform.io/providers/hashicorp/google/4.47.0
      version = "~> 4.47.0"
    }
  }
}
