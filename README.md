# What?

This repository performs one of two tasks, depending on how it is consumed:

* runs [a creation wizard](#cloud-shell-wizard) inside of Google Cloud Shell, for a more interactive experience
* serves as [a terraform module](#terraform-module) for use with any existing automation

It will create 3 resources in your Google Cloud enviroment (as shown graphically below):
* a new Project, with certain Google Cloud APIs enabled and a Billing Account assigned to it
* a new Project-scoped IAM Role
* an Organization-scoped IAM Role

```mermaid
graph RL
subgraph "Open Raven"
  gcpSA(your dedicated Service Account)
end
subgraph "Your GCP Environment"
  subgraph "Org1"
    newRole(newly created IAM Role)
    style newRole stroke:red,stoke-width:5px;
    newRole -- granted to for discovery --> gcpSA
    subgraph "Folder 1"
      proj1(Project 1)
      proj2(Project 2...etc)
    end
    proj3(Project 3...etc)
    subgraph projX [newly created project]
      noteX[/"#nbsp; also assigned a billing account"/]
      style noteX color:#808080;
      projX_role(Project IAM Role)
      projX_role -- granted to for scanning --> gcpSA
      style projX_role stroke:red,stoke-width:5px;
    end
    style projX stroke:red,stoke-width:5px;
  end
end
```

## New Project

We create a new Project in your Google Cloud environment for two reasons:
1. It enables easy managemnet of the billing
1. It allows enabling APIs without having to enable them on Projects that other infrastructure may already be managing

## New Project IAM Role

We create a new Project-scoped IAM Role in order to confine the "executable" actions that Open Raven will make inside your Google Cloud environment.

This new IAM Role will trust the same dedicated Service Account inside Open Raven as the IAM Role below.

## New Organization IAM Role

We create a new Organization-scoped IAM Role in order to gain visibility into your Google Cloud Organization. This new IAM Role will trust a dedicated Service Account inside Open Raven used only by your Open Raven workspace. It is granted read-only permissions.

# Cloud Shell Wizard

You will want to click "Trust Repo" when that html dialog appears in your Google Cloud Shell,
in order to allow the `gcloud` command inside the Cloud Shell to access your Google Cloud credentials.
Without this, `gcloud auth list` will ask you to `gcloud auth login` which is not as nice of an experience.

Be aware that while Cloud Shell is free of charge:

> Cloud Shell has weekly usage limits. If you reach these limits you will need to wait before you can use Cloud Shell again.
>
> 0 of 168 hours used

[![Open this project in Cloud Shell](http://gstatic.com/cloudssh/images/open-btn.png)](https://console.cloud.google.com/cloudshell/open?git_repo=https%3A%2F%2Fgitlab.com%2Fmdaniel%2Fgcp-shell-0&page=editor&tutorial=getting-started.md&show=terminal)

# Terraform Module

This same repo provides a Terraform module, if you already have the resources to consume that.

```hcl
module "openraven" {
    source = "git::https://gitlab.com/openraven/open/gcp-terraform.git//gcp"

    gcp_billing_account_id = var.your_GCP_billing_account_id // AAAAAA-BBBBBB-CCCCCC
    gcp_org_id             = var.your_GCP_organization_id // the 000011112222 one
    gcp_folder_id          = var.your_GCP_folder_id // or null to place the new Project outside of a Folder
    openraven_sa_email     = var.the_sa_email_from_openraven
    openraven_org_id       = var.the_Org_Id_provided_by_openraven
}
```
